#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 97000

typedef struct data{
  int size;
  char *word;
  int a[26];
  // int *pa;
}data;

data rdata[SIZE];


void read(char *filename){/*ファイルから辞書データを読み込む*/
  FILE *file;
  char *rword;//単語が入っている配列のポインタ
  int size;  //単語の文字数
  int i=0;//for文で使う
 
  file = fopen(filename,"r"); //ファイルを読み込み専用で開く
  if(file == NULL){ //ファイルが見つからなかったら
    printf("ファイル%sが見つかりません\n",filename);
    exit(1);  //エラーを出して終了
  }

 //ファイルからデータを読み込み、構造体に格納
    fread(&rdata,sizeof(data),1,file);
  

  fclose(file);//ファイルを閉じる

  return ;

}


main(int argc , char argv[]){
  int g=0,h=0,i=0,j=0,k=0,l=0,hikaku=0,bit=0,count=0;
  int w,z;
  int m[2];
  int array[26],n[26];
  char anag[16];
  char moji[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
  int a[26];
  //data *p;

  if(argc!=16){
    printf("16文字入力してください\n");
    exit(1);
  }

  for(h;h<26;h++){//arrayとaを初期化しておく
    array[h]=0;
    a[h]=0;
  }

  // srand(time(NULL));//プログラムを起動するたびにrand()が違う値を返すように

  //for(k;k<16;k++){
  // argv[k]='a'+rand%('z'-'a');//ランダム関数でa~zのうちから16文字を取ってきて、配列に格納
  // }
  // printf("Find Anagram!!\n");
  // puts(argv); //16文字の表示

    while(argv[i]!='0'){//与えられた16文字を最後まで見ていく
      while(moji[j]!='0'){//アルファベットをaからzまで見ていく
	if(argv[i]==moji[j]){//文字とアルファベットが一致したら
	  array[j]+=1;//1増やす
	  break;//内側のwhile文を抜ける
	}else{
	  j++;//一致しなければ次のアルファベットへ移動
	}
      }
      i++;//次の文字へ
    }

    read("jisho.dat");//jisho.cで作った、新しい辞書を読み込む
  
    //p = rdata;//ポインタ値の設定

    for(l;l<SIZE;l++){//辞書の頭から最後まで見ていく
      //p=rdata;
      for(g;g<26;g++){
	a[g]=rdata[l].a[g];
      }
      printf("%s\n",a);
      for(hikaku;hikaku<26;hikaku++){//比較していく
	if((a[hikaku]-array[hikaku])>=0){//引いて正であるところをみつけたら、
	  n[hikaku]=1;//それをnにメモしておく
	}
      }

      for(bit;bit<26;bit++){
	if(n[bit]==1){//比較の結果の配列を見ていき、"1"を見つけたら、
	  count++;//その数をカウントしていく
	}
      }

      z=rdata[l].size;
      if(count>=z && z>m[0]){//単語が見つかり、かつ前に見つけた単語より文字数が多かったら
	m[0]=z;//配列の1番目に文字数を、
	m[1]=l;//2番目に辞書内で何番目の単語かをメモしておく
      }
    }
    w=m[1];//最終的に選ばれた最も長い単語の番号をwに格納
    printf("The answer is %s",rdata[w].word);//単語を出力して

    char *word2;
    word2=rdata[w].word;

}//終了
