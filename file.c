#include<stdio.h>
#include<stdlib.h>

#define SIZE 5

int main(){
  FILE *fp1,*fp2;
  char *fname1="input_0.csv",*fname2="mysolution_0.csv";
  double x[SIZE],y[SIZE];
  int i=0,j=0,ret;
  char a,b;

  fp1=fopen(fname1,"r");
  if(fp1==NULL){
    printf("ファイルを開けません\n");
    return -1;
  }

  fscanf(fp1,"%c,%c",&a,&b);

  while((ret=fscanf(fp1,"%lf,%lf",&x[i],&y[i]))!=EOF){
    printf("%lf %lf\n",x[i],y[i]);
    i++;
  }

  fclose(fp1);


  fp2=fopen(fname1,"w");
  if(fp2==NULL){
    printf("ファイルを開けません\n");
    return -1;
  }
  
  for(j;j<SIZE;j++){
    fprintf(fp2,"%lf,%lf\n",&x[j],&y[j]);
  }

  fclose(fp2);
}
