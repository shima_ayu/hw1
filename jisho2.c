#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 1000000//辞書の単語格納するにはこれで十分みたい！


int n=0;//格納するデータの数


char word[SIZE][16];
int a[SIZE][26];
int s[SIZE];

void read(char *filename){//ファイルから辞書を読み込む
  FILE *file;
  int size,cnt=0,num=0;
  int a;
  int i=0,j=0;
 

  file = fopen(filename,"r");//ファイルを読み込み専用で開く
  if(file==NULL){//ファイルが見つからなかったら
    printf("ファイル%sが見つかりませんでした",filename);
    exit(1);//エラーを出して終了
  }
  while(1){//以下をデータがなくなるまで行う
    a = fscanf(file,"%s\n",word[n]);//1行読み込む
    if(a==EOF){//ファイルの終わりなら
      fclose(file);//ファイルを閉じて
      return;//終了
    }else{//ファイル内にまだデータがあったら
      size=0;//size初期化
      if(word[n][0]>=97 && word[n][0]<=122){//最初の文字が小文字なら
	while(word[n][size]!='\0'){//wordを最後まで読み込むまで続ける
	  size++;//文字数をカウント
	}
	s[n]=size;
	//printf("%d %s\n",size,word[n]);//文字数と単語を出力してみる
	
	n++;//次の単語へ
      }
    }
  }
}//read終了
  
  

int check(){//rwordに含まれるすべての単語について、a~zのうちどの文字でできているかをチェックし、配列aに格納する関数
  int h=0,i=0,j=0,k=0,l=0;
  char moji[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};//mojiにa~zを格納
  // int b[SIZE][26];
 //aの中には、全部０を入れて準備しておく
  for(h;h<n;h++){
    for(i;i<26;i++){
      a[h][i]=0;
    }
    // printf("%d\n",a[h]);
  }
  // printf("\n");
  // printf("%s\n",rword[0]);
  while(j<=n){//辞書内のデータの最後まで繰り返す
    k=0;
    while(word[j][k]!='\0'){//rwordに格納した単語の最後の文字まで見ていく
      for(l=0;l<26;l++){//単語中の１文字が該当するアルファベットが見つかるまで
	if(word[j][k]==moji[l]){
	  a[j][l]+=1;//今見ている単語の、今一致したアルファベットのところの数字に1を足す
	  break;//内側のwhile文を抜ける
	}
      }
      k++;//単語の次の文字へ
    }
    //printf("%d\n",a[j]);
    j++;//次の単語へ
  }
  return;
}


void write(){//出来上がった配列aをバイナリデータとして新しい辞書を作る
  FILE *file;
  int h=0,i=0,j=0,size=0;
  
  file = fopen("jisho.csv","w");
  if(file==NULL){//エラー処理
    printf("ファイルjisho.csvを開くのに失敗しました\n");
    exit(1);
  }
  for(i;i<n;i++){
    for(j=0;j<26;j++){
      fprintf(file,"%d",a[i][j]);
    }
    fprintf(file,",%d,%s\n",s[i],word[i]);
  }
 
  printf("配列,　　文字数,　　単語 \n");
  
  fclose(file);//ファイルを閉じる
  
  
}
 
void main(){
   int i=0,size=0;
   //printf("ここまでできている\n");ok
   read("/usr/share/dict/american-english");//read関数の実行
   printf("%d\n",n);
   check();//check関数の実行
   //printf("ここまでできている\n");ok
   write();
   printf("辞書を作成しました\n");

   return;
}


