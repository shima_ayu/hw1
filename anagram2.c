#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 1000000

char word[SIZE][16];
char a[SIZE][26];
int a1[SIZE][26];
int s[SIZE];
int nn=0;
int mm=0;

void read(char *filename){/*ファイルから辞書データを読み込む*/
  FILE *file;
  int ret,h,i=0,j,k;//for文で使う
 
  file = fopen(filename,"r"); //ファイルを読み込み専用で開く
  if(file == NULL){ //ファイルが見つからなかったら
    printf("ファイル%sが見つかりません\n",filename);
    exit(1);  //エラーを出して終了
  }

 //ファイルからデータを読み込み格納
  while((ret=fscanf(file,"%[^,],%d,%s",a[i],&s[i],word[i]))!=EOF){
    for(j=1;j<27;j++){
      char tmp[2];
      tmp[0]=a[i][j];
      tmp[1]='\0';
      a1[i][j]=atoi(tmp);
      //  printf("%d",a1[nn][j]);
    }
    //  printf(",s=%d,word=%s  ",s[nn],word[nn]);
    i++;
    nn++;
  }
   printf("%d\n",nn);
  fclose(file);//ファイルを閉じる

  return ;

}

main(){
  int ret1=0,ret2=0,f=0,g=0,h=0,i=0,j=0,k=0,l=0,hikaku=0,bit=0,count=0;
  int w,z;
  int m[2];
  int array[26],n[26];
  char anag[16],argv[16];
  char moji[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
  int a[26];
  //data *p;

  printf("please input 16 characters\n");
  while((argv[f]=getchar())!='\n'){
    //printf("argv[%d]=%c\n",f,argv[f]);
    f++;
  }
  if(f!=16){
    printf("please input 16 characters!!!!\n");
    exit(1);
  }
  argv[16]='\0';

  for(h;h<26;h++){//arrayを初期化しておく
    array[h]=0;
  }
  
  while(argv[i]!='\0'){//与えられた16文字を最後まで見ていく
    j=0;
    while(moji[j]!='\0'){//アルファベットをaからzまで見ていく
      if(argv[i]==moji[j]){//文字とアルファベットが一致したら
	array[j]+=1;//1増やす
	break;//内側のwhile文を抜ける
      }else{
	j++;//一致しなければ次のアルファベットへ移動
	}
    }
    i++;//次の文字へ
  }
  for(j=0;j<26;j++){
    printf("%d",array[j]);
  }
  printf("\n");

  read("jisho.csv");//jisho2.cで作った、新しい辞書を読み込む
  printf("%d\n",nn);
  /* for(mm;mm<nn;mm++){
   printf("a1=%d,s=%d,word=%s  ",a1[mm],s[mm],word[mm]);
   }*/
  m[0]=0;
  for(g=0;g<nn;g++){
    // printf("%s  ",word[g]);
    ret1=0;
    ret2=0;
    /*  int aa;
    for(aa=0;aa<26;aa++){
      printf("%d",a1[g][aa]);
    }
    printf(",%s\n",word[g]);*/
    for(hikaku=0;hikaku<26;hikaku++){//比較していく
      ret1=array[hikaku]-a1[g][hikaku+1];
      if(ret1<0){//引いて負であるところをみつけたら、
	//	printf("ret1=%d,word=%s\n",ret1,word[g]);
	break;
      }
      if(hikaku==25){
	//	printf("%s\n",word[g]);
	if((ret2=(s[g]-m[0]))>0){
	  m[0]=s[g];
	  m[1]=g;
	  // printf("%s\n",word[g]);
	}
      }
    }
  }
  
  w=m[1];//最終的に選ばれた最も長い単語の番号をwに格納
  printf("The answer is %s(size=%d)\n",word[w],s[w]);//単語を出力して
  
  
}//終了
