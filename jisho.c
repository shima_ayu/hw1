#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 100000//辞書の単語格納するにはこれで十分みたい！

typedef struct data{
  int size;//単語の文字数
  char word[16];//単語
  int a[26];
}data;

data table[SIZE];//大きさSIZEのdata型配列の宣言
int n=0;//tableに格納するデータの数


void insert(int x,char w[15]){//tableの最後にデータ（単語）を追加
  table[n].size=x;//n番目に文字数と
  table[n].word[16]=w[16];//単語を入れて
  n++;//nを増やす
}




void read(char *filename){//ファイルから辞書を読み込む
  FILE *file;
  int size,cnt=0,num=0;
  int a;
  int h=0,i=0,j=0;
  char rword[SIZE][16];

  file = fopen(filename,"r");//ファイルを読み込み専用で開く
  if(file==NULL){//ファイルが見つからなかったら
    printf("ファイル%sが見つかりませんでした",filename);
    exit(1);//エラーを出して終了
  }
  //printf("ここまでできている\n");ok!!!
  while(1){//以下をデータがなくなるまで行う
    a = fscanf(file,"%s\n",rword[h]);//1行読み込む
    // printf("ここまでできている\n");ok
    if(a==EOF){//ファイルの終わりなら
      fclose(file);//ファイルを閉じて
      return;//終了
    }else{//ファイル内にまだデータがあったら
      for(j;j<15;j++){
	printf("%c",rword[h][j]);
	table[num].word[j]=rword[h][j];//tableのword配列にその単語を１文字ずつ格納
	//printf("%s\n",table[num].word);
      }
      num++;
      size=0;
      while(rword[h][size]!='\0'){//rwordを最後まで読み込むまで続ける
	size++;//文字数をカウント
      }
      printf("%d",size);
      printf(" ");
      printf("%s\n",rword[h]);
      insert(size,rword[h]);//文字数と単語をtableに追加
    }
    //cnt++;
    //printf("%d\n",cnt);
  }
  h++;
}//read終了



int check(){//tableに含まれるすべての単語について、a~zのうちどの文字でできているかをチェックし、arrayに格納する関数
  int h=0,i=0,j=0,k=0,l=0;
  char moji[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};//mojiにa~zを格納
  char word[15];
 //data型のtable.aの中には、全部０を入れて準備しておく
  for(h;h<SIZE;h++){
    for(i;i<26;i++){
      table[h].a[i]=0;
    }
  }

  while(j==n){//辞書内のデータの最後まで繰り返し
    word[15] = table[j].word[15];//wordにtable内のj番目の単語を格納

    while(word[k]!='0'){//wordに格納した単語の最後の文字まで見ていく
      if(word[k]==moji[l]){//単語中の１文字が該当するアルファベットが見つかったら、
	table[j].a[l]+=1;//今見ている単語の、今一致したアルファベットのところの数字を0から1を足す
	break;//内側のwhile文を抜ける
      }else{//そうでなかったら
	l++;//次のアルファベットへ
      }
    }
    //printf("%s\n",word);
    k++;//次の文字へ
  }

  j++;

  return;

}


void write(data table[n]){//出来上がった配列aをバイナリデータとして新しい辞書を作る
  FILE *file;
  int h=0,i=0,j=0,size=0;
  char word[15];

  file = fopen("jisho.dat","wb");//バイナリデータとして書き込み
  if(file==NULL){//エラー処理
    printf("ファイルjisho.txtを開くのに失敗しました\n");
    exit(1);
  }

  for(j;j<n;j++){
    size=table[j].size;
    for(h;h<15;h++){
      word[h]=table[j].word[h];
    }
    //printf("%d %s %s\n",size,word,table[j].a);
  }
  //printf("%s\n",word);
  //printf("文字数、　単語、　配列 \n");
  for(i;i<n;i++){
    fwrite(&table[i],sizeof(data),1,file);//ファイルにtableのデータを書き込む
  }

  fclose(file);//ファイルを閉じる


}

main(){
  int i=0;
  //printf("ここまでできている\n");ok
  read("/usr/share/dict/american-english");//read関数の実行
  //printf("ここまでできている\n");ok
  check();//check関数の実行
  //printf("ここまでできている\n");ok
  write(table);
  printf("辞書を作成しました\n");
}


