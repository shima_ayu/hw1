#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

struct Trie {
  bool leaf;
  Trie* node[256];
  Trie(){
    leaf = false;
    for(int i=0; i<256; i++){
      node[i] = (Trie*)0;
    }
  }
  void insert(const string &str){
    Trie *r = this;
    for(int i=0; i<str.length(); i++){
      char c = str[i];
      if(!r->node[c]) r->node[c] = new Trie;
      r = r->node[c];
    }
    r->leaf = true;
  }
  bool find(const string &str) const {
    const Trie *r = this;
    for(int i=0; i<str.length(); i++){
      char c = str[i];
      if(!r->node[c]) return false;
      r = r->node[c];
    }
    return r->leaf;
  }    
};

void check(const Trie &t, const string &str){
  cout << str << ": ";
  if(t.find(str)) cout << "find" << endl;
  else cout << "not find" << endl;
}


int main(){
  Trie trie;
  trie.insert("an");
  trie.insert("ant");
  trie.insert("all");
  trie.insert("allot");
  trie.insert("alloy");
  trie.insert("aloe");
  trie.insert("are");
  trie.insert("ate");
  trie.insert("be");

  check(trie, "an");
  check(trie, "all");
  check(trie, "allow");
  check(trie, "aiueo");
  check(trie, "are");
  check(trie, "bc");
  
  return 0;
}
